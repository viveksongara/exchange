@extends('layouts/frontend/master')
@section('content')	
    
    <div class="main-area">
        <div class="container">
            <div class="row">
               
                <div class="col-sm-12">
                    <div class="panel panel-default mt20">
                        <div class="panel-heading">
                            <h3 class="panel-title">Vendor Dashboard</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class='well well-lg text-warning'>
                                        <h2 class='text-center'><i class='fa fa-shopping-cart fa-2x'></i></h2>
                                        <p class='text-center'>Products Pending : {{$product_pending}}</p>
                                    </div>

                                </div>   
                                <div class="col-sm-4">
                                    
                                    <div class='well well-lg text-success'>
                                        <h2 class='text-center'><i class='fa fa-shopping-cart fa-2x'></i></h2>
                                        <p class='text-center'>Products Accepted : {{$product_accepted}}</p>
                                    </div>

                                </div>
                                <div class="col-sm-4">
                                    
                                    <div class='well well-lg text-danger'>
                                        <h2 class='text-center'><i class='fa fa-shopping-cart fa-2x'></i></h2>
                                        <p class='text-center'>Products Rejected : {{$product_rejected}}</p>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                  
                </div>

               
            </div>
            <!--row-->
            </div>
        <!--container-->
    </div>
    <!-- main area end -->
    @endsection
