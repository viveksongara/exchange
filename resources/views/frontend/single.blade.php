@extends('layouts/frontend/master')
@section('content')	
    
    

    <!-- creative banner area start -->
    
    <!-- creative banner area end -->
    
    <!-- main area start -->
    <div class="main-area">
        <div class="container">
            <div class="row">
                
                <!--col-md-3-->
                <div class="col-md-9 col-sm-9 nopadding-left">
                    <div class="ambit-key">
                        <div class="col-md-12 pt40">
                            <ol class="breadcrumb">
                                <li class="home"><a href="index.html" title="Go to Home Page">Home</a></li>
                                <li class="active">Single-product</li>
                            </ol>
                            <div class="shop-banner mb20 mt20 ">
                                @foreach($products as $product)
                                    <tr>
                                        <td>
                                            <img style="max-width:150px;" class="img img-responsive" src="{{asset('assets/frontend/img/product').'/'.$product['product_image']}}">
                                        </td>
                                        
                                       
                                      
                                        
                                    </tr>

                                   
                                
                            </div>
                        </div>
                        <!-- product-details-start-->
                        <div class="product-single-details">
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <div class="product-details-content">
                                    <h3>{{$product['product_title']}}</h3>
                                    <div class="hits-rating">
                                        
                                    <a href="#"><i class="fa fa-star"></i></a>
                                    <a href="#"><i class="fa fa-star"></i></a>
                                    <a href="#"><i class="fa fa-star"></i></a>
                                    <a href="#"><i class="fa fa-star"></i></a>
                                    <a href="#"><i class="fa fa-star-o"></i></a></span></p>
                                    </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <div class="features-tab product-des-review">
                                <!-- Nav tabs -->
                                <ul class="nav">
                                    <li role="presentation" class="active"><a href="#product-des" data-toggle="tab">Product Descripton</a></li>
                                    <li role="presentation"><a href="#add-review" data-toggle="tab">Add Your Review</a></li>
                                    <li role="presentation"><a href="#product-tag" data-toggle="tab">Product Tags</a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade in active" id="product-des">
                                        <div class="std">
                                            <p><strong>Product Description</strong></p>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur r idiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.</p>
                                            <p>At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum d olor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diumy eirmod tempor invidunt et dolore</p>
                                            
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade in" id="add-review">
                                        <div class="review-upper">
                                            <div class="review_item">
                                                <div><span class="review_user"><strong>Canada Goose Sale</strong></span>, <span class="review_time">10.10.2015</span></div>
                                                <div class="review_text">
                                                    <p>Best Buy cheap rolex watches, imitation rolex watches, discount watch store, discount watch, replica rolex, rolex replica, fake rolex, fake watches, Rolex Replica, rolex Replica For Sale, calvin klein outlet, calvin klein underwear outlet, calvin klein underwear, calvin klein underwear outlet, calvin klein women underwear, calvin klein underwear for women, calvin klein underwear men, calvin klein underwear for men, calvin klein underwear men, men calvin klein, Christian Louboutin, Canada Goose Parka, Louis Vuitton Online Shop, Michael Kors Outlet Store , Michael Kors Bags On Sale, cheap jerseys, Canada Goose Jacket</p>
                                                    <span> Canada Goose Sale: <a href="#">http://www.gamlielrallyteam.com</a></span>
                                                </div>
                                            </div>
                                            <div class="reviewer-ratting">
                                                <div class="name-date">
                                                    <strong>Yasuo</strong><span>17.11.2025</span>
                                                </div>
                                                <span class="best">Best</span>
                                                <div class="rating">
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                    <a href="#"><i class="fa fa-star-o"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="review-lower mt30">
                                            <div class="input-header">
                                                <p>Write a comment for this product!</p>
                                            </div>
                                           <form method='post' enctype="multipart/form-data" action="{{url('user/make-feedback')}}">
                                                {{csrf_field()}}
                                                <div class="form-group">
                                                    <label for="input">name</label>
                                                    <input name="name" value="{{old('name')}}" type="text" class="form-control" id="name" placeholder="Text">
                                                </div>
                                                     <td><a  class="btn btn-danger btn-sm"href="{{url('user/make-feedback').'?product_id='.$product['product_id']}}">Submit</a></td>
                                                       
                                                    </a>
                                                </div>
                                            </form>
                                             @endforeach
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade in" id="product-tag">
                                        <p>Comming Soon </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        
                       
                    <!--ambit-key-->
                </div>
                <!--col-md-9-->
            </div>
            <!--row-->
        </div>
        <!--container-->
</div>
    <!-- main area end -->

   
    
 
    @endsection
