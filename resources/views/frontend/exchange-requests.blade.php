@extends('layouts/frontend/master')
@section('content')	
    
    <div class="main-area">
        <div class="container">
            <div class="row">
               
                <div class="col-sm-12">
                    
                    <div class="panel panel-default mt20">
                        <div class="panel-heading">
                            <h3 class="panel-title">Exchange Requests</h3>
                        </div>
                        <div class="panel-body">
                            @if(Session::has('message'))
                            <div class='alert alert-success'>
                                {{Session::get('message')}}
                            </div>
                            @endif
                            
                            @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Product Image</th>
                                        <th>Product (Yours)</th> 
                                        <th>Product (to be exchanged)</th>
                                       
                                        <th class='text-center'>Status</th>
                                        <th class="text-center">#</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($products as $product)
                                    <tr>
                                        <td>
                                            <img style="max-width:50px;" class="img img-responsive" src="{{asset('assets/frontend/img/product').'/'.$product['product_image']}}">
                                        </td>
                                        <td>{{$product['product_title']}}</td>
                                        <td><a  class="btn btn-danger btn-sm"href="{{url('single').'?product_id='.$product['product_id']}}">{{$product['product_two_title']}}</a></td> </td>
                                       
                                        
                                        <td class='text-center'>
                                            @if($product['is_accepted']==1)
                                            <span class='text-success'>Approved</span>
                                            @elseif($product['is_accepted']=2)
                                            <span class='text-danger'>Rejected</span>
                                            @else
                                            <span class='text-primary'>Pending</span>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            <a class="btn btn-sm btn-success" href="{{url('user/approve-exchange').'?exchange_id='.$product['exchange_id']}}"><i class='fa fa-check'></i></a>
                                            <a class="btn btn-sm btn-danger" href="{{url('user/reject-exchange').'?exchange_id='.$product['exchange_id']}}"><i class='fa fa-remove'></i></a>
                                        </td>
                                    </tr>

                                    @endforeach
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                  
                </div>

               
            </div>
            <!--row-->
            </div>
        <!--container-->
    </div>
    <!-- main area end -->
    @endsection
