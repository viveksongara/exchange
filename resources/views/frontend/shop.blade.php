   @extends('layouts/frontend/master')
   @section('content')

    <!-- creative banner area start -->
    
    <!-- creative banner area end -->

    <!-- main area start -->
    <div class="main-area">
        <div class="container">
            <div class="row">
                

                <!--col-md-3-->
                <div class="col-md-12 col-sm-9 nopadding-left">
                    <div class="ambit-key">
                        <div class="col-md-12 pt40">
                            <ol class="breadcrumb">
                                <li class="home"><a href="index.html" title="Go to Home Page">Home</a></li>
                                <li class="active">Shop</li>
                            </ol>
                            <div class="shop-banner mt20 mb20">
                              
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="shop-product-area">
                            <!-- area title start -->
                            <div class="col-sm-12">
                                <div class="area-title bdr">
                                    <h2>Items Available</h2>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <!-- area title end -->
                            <div class="short-area mt20">
                                
                            </div>
                            <!-- product area start-->
                            <div class="row row-margin2">
                                <div class='col-sm-12'>
                                @if(Session::has('message'))
                                <div class='alert alert-success'>
                                    {{Session::get('message')}}
                                </div>
                                @endif
                                
                                @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                
                                </div>
                                @foreach($products['data'] as $product)
                                
                                <!-- single-product start -->
                                <div class="col-lg-3 col-md-4  col-sm-6  col-xs-12  col-padd">
                                    <div class="single-product">
                                        <div class="product-label">
                                           
                              
                                           
                                           
                                        </div>
                                        <div class="product-img">
                                            <a href="{{url('product-details').'?product_id='.$product['product_id']}}">
                                                <img class="primary-image" src="{{asset('assets/frontend/img/product').'/'.$product['product_image']}}" alt="" />
                                            </a>
                                        </div>
                                        <div class="product-content">
                                            <h2 class="product-name"><a href="#">{{$product['product_title']}}</a></h2>
                                            <h2 class="product-name">
                                                <a href="#">Offerred By :<br>
                                                    
                                                    <span><img class="img img-responsive img-rounded pull-left" style="width:30px" src="{{asset('assets/pics/profile_picture').'/'.$product['profile_picture']}}"> </span>
                                                    <span class="pull-left">{{$product['name']}}</span>
                                                </a>
                                            </h2>
                                                    <div class="clearfix"></div>
                                                    <div class="rating">
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <a href="#"><i class="fa fa-star-o"></i></a>
                                            </div>
                                            <div class="price-box">
                                                
                                            </div>
                                        </div>
                                        <div class="product-content2">
                                            <h2 class="product-name"><a href="#">{{$product['product_title']}}</a></h2>
                                            <div class="rating">
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <a href="#"><i class="fa fa-star-o"></i></a>
                                            </div>
                                            <div class="price-box">
                                                <span class="new-price"></span>
                                            </div>
                                            <div class="button-container">
                                                @if(Auth::check())
<!--                                                <a title="Add to Cart" href="{{url('user/apply-product-for-exchange').'?product_id='.$product['product_id']}}" class="button cart_button">
                                                    <span>Apply</span>
                                                </a>-->
                                                    @if(count($items_to_offer) == 0)
                                                    <p style='color:red;'>
                                                        You don't have products to exchange
                                                        <a target='_blank' style='color:blue;' href='{{url('/user/post-product')}}'>Post now</a>
                                                    </p>
                                                    @else
                                                    <form method='get' enctype="multipart/form-data" action="{{url('user/apply-product-for-exchange')}}">
                                                        <input type='hidden' name='product_id' value='{{$product['product_id']}}'>   
                                                        <div class="form-group">

                                                            <select name="product_two_id" class="form-control">
                                                                <option value="">Items to offer </option>
                                                                @foreach($items_to_offer as $item)
                                                                <option @if(old('product_two_id') == $item['product_id']) selected @endif value="{{$item['product_id']}}">{{$item['product_title']}}</option>
                                                                @endforeach

                                                            </select>
                                                            <input class="button cart_button" type="submit" value="Make an offer">
                                                        </div>
                                                    </form>
                                                    

                                                    @endif
                                                

                                                @else
                                                
                                                <a title="Add to Cart" href="{{url('login')}}" class="button cart_button">
                                                    <span>Login to Apply</span>
                                                </a>

                                                @endif
                                                
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <!-- single-product end -->

                                @endforeach
                                
                               
                            </div>
                        </div>
                        <!--shop product area end-->
                        <div class="col-md-12">
                            
                                <ul class="pagination">
                                <li class="@if($products['prev_page_url'] == '') disabled @else @endif"><a  href="{{$products['prev_page_url']}}">Previous</a></li>
                                <?php
                                $last_page = $products['last_page'];
                                $i = 1;
                                ?>
                                @while($i <= $last_page)
                                <li class="@if($products['current_page'] == $i) disabled @else @endif"><a href="{{url('shop').'?page='.$i}}">{{$i}}</a></li>
                                <?php $i++; ?>
                                @endwhile
                                <li class="@if($products['next_page_url'] == '') disabled @else @endif"><a href="{{$products['next_page_url']}}">Next</a></li>
                            </ul>
                            
                            
                            
                        </div>
                        <!--brand crasoule-area-start-->
                        <div class="brand-crasoule-area featur-padd">
                            <!-- area title start -->
                            
                        </div>
                        <!--brand crasoule-area-end-->
                    </div>
                    <!--ambit-key-->
                </div>
                <!--col-md-9-->
            </div>
            <!--row-->
        </div>
        <!--container-->
</div>
    <!-- main area end -->
    
    @endsection

    