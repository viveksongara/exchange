@extends('layouts/frontend/master')

@section('content')

<div class="main-area">
        <div class="container">
            <div class="row">
    <div class="col-sm-6" style='margin-top:50px; margin-bottom:50px;'>
        @if(Session::has('message'))
        <div class='alert alert-success'>
            {{Session::get('message')}}
        </div>
        @endif
        
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        
        <form method='post' enctype="multipart/form-data" action="{{url('user/make-feedback')}}">
            {{csrf_field()}}

            <div class='well wel-sm'>
                <label for="input">Provide Rating : </label>
        <label class="radio-inline">
          <input  type="radio" name="rating" id="inlineRadio1" value="1"> 1
        </label>

        <label class="radio-inline">
          <input type="radio" name="rating" id="inlineRadio1" value="2"> 2
        </label>
        <label class="radio-inline">
          <input type="radio" name="rating" id="inlineRadio1" value="3"> 3
        </label>
        <label class="radio-inline">
          <input type="radio" name="rating" id="inlineRadio1" value="4"> 4
        </label>
         <label class="radio-inline">
          <input type="radio" name="rating" id="inlineRadio1" value="5"> 5
        </label>
                
            </div>
        

  
        <div class="form-group">
            <label for="input">Feedback Message </label>
            <textarea required name='feedback' class="form-control" rows="3">{{old('feedback')}}</textarea>
        </div>
        <input type='hidden' name='received_by' value='{{$received_by}}'>
        <input type='hidden' name='given_by' value='{{$given_by}}'>
        <input class="btn btn-success" type="submit" value="Provide Feedback">
    

</form>
    </div>    
</div>
            <!--row-->
            </div>
        <!--container-->
    </div>

 



@endsection