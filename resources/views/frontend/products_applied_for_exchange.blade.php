@extends('layouts/frontend/master')
@section('content')	
    
    <div class="main-area">
        <div class="container">
            <div class="row">
               
                <div class="col-sm-12">
                    
                    <div class="panel panel-default mt20">
                        <div class="panel-heading">
                            <h3 class="panel-title">Products Applied for exchange</h3>
                        </div>
                        <div class="panel-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Product Title</th>
                                        <th>Vendor Name</th>
                                        <th>Product images</th>
                                        <th>Offer Date</th>
                                        <th>Offer Time</th>
                                        <th>Status</th>
                                        <th class='text-center'>Feedback</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($products as $product)
                                    
                                    <tr>
                                        <td>{{$product['product_title']}}</td>
                                        <td>{{$product['name']}}</td>
                                        <td> <img class="primary-image" style="width:80px" src="{{asset('assets/frontend/img/product').'/'.$product['product_image']}}" alt="" /></td>
                                        <td>{{$product['offer_date']}}</td>
                                        <td>{{$product['offer_time']}}</td>
                                        <td>
                                            @if($product['is_accepted']==0)                                          
                                            
                                            <span class='text-primary'>Pending</span>

                                            @endif
                                            
                                            @if($product['is_accepted']==1)                                          
                                            
                                            <span class='text-success'>Success</span>

                                            @endif
                                            
                                            @if($product['is_accepted']==2)                                          
                                            
                                            <span class='text-danger'>Danger</span>

                                            @endif
                                            
                                            
                                            
                                            
                                            
                                            
                                        </td>
                                        <td class='text-center'>
                                            @if($product['is_accepted'] == 1)
                                            <a class="btn btn-sm btn-warning" href="{{url('user/feedback').'?exchange_id='.$product['exchange_id'].'&user_id='.$product['user_id']}}"><i class="fa fa-star"></i> Give feedback</a>
                                            @else
                                            <span>#</span>
                                            @endif
                                            
                                        </td>
                                    </tr>

                                    @endforeach
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                  
                </div>

               
            </div>
            <!--row-->
            </div>
        <!--container-->
    </div>
    <!-- main area end -->
    @endsection
