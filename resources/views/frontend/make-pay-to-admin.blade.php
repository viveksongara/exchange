@extends('layouts/frontend/master')
@section('content')	
    
    <div class="main-area">
        <div class="container">
            <div class="row">
               
                <div class="col-sm-12">
                    
                    <div class="panel panel-default mt20">
                        <div class="panel-heading">
                            <h3 class="panel-title">Payment</h3>
                        </div>
                        <div class="panel-body">
                            
                            <table class="table">
                                <thead>
                                    
                                </thead>
                                <tbody>
                                    <tr>
                                        <th>Product Title</th>
                                        <td>{{$exchanges[0]['product_title']}}</td>
                                    </tr>
                                    
                                    <tr>
                                        <th>Vendor name</th>
                                        <td>{{$exchanges[0]['name']}}</td>
                                    </tr>
                                </tbody>
                            </table>
                            
                            <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
                                <input type="hidden" name="cmd" value="_xclick">
                                <input type="hidden" name="business" value="priyank086@gmail.com">
                                <input type="hidden" name="item_name" value="{{$exchanges[0]['product_title']}}">
                                <input type="hidden" name="item_number" value="1">
                                <input type="hidden" name="amount" value="10">
                                <input type="hidden" name="no_shipping" value="0">
                                <input type="hidden" name="no_note" value="1">
                                <input type="hidden" name="currency_code" value="USD">
                                <input type="hidden" name="lc" value="IN">
                                <input type="hidden" name="bn" value="PP-BuyNowBF">
                                <input type="image" src="https://www.paypal.com/en_AU/i/btn/btn_buynow_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online.">
                                <img alt="" border="0" src="https://www.paypal.com/en_AU/i/scr/pixel.gif" width="1" height="1">
                            </form>
                            
                        </div>
                    </div>
                    
                  
                </div>

               
            </div>
            <!--row-->
            </div>
        <!--container-->
    </div>
    <!-- main area end -->
    @endsection
