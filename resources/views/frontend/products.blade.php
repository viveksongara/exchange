@extends('layouts/frontend/master')
@section('content')	
    
    <div class="main-area">
        <div class="container">
            <div class="row">
               
                <div class="col-sm-12">
                    
                    <div class="panel panel-default mt20">
                        <div class="panel-heading">
                            <h3 class="panel-title">My Products as vendor</h3>
                        </div>
                        <div class="panel-body">
                            @if(Session::has('message'))
                            <div class='alert alert-success'>
                                {{Session::get('message')}}
                            </div>
                            @endif
                            
                            @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Product Image</th>
                                        <th>Title</th>
                                        <th>Category</th>
                                        <th>Price</th>
                                        <th class="text-center">#</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($products as $product)
                                    <tr>
                                        <td>
                                            <img style="max-width:50px;" class="img img-responsive" src="{{asset('assets/frontend/img/product').'/'.$product['product_image']}}">
                                        </td>
                                        <td>{{$product['product_title']}}</td>
                                        <td>{{$product['category_name']}}</td>
                                        <td>{{$product['price']}}</td>
                                        <td class="text-center">
                                            <a class="btn btn-sm btn-danger" href="{{url('user/delete-product').'?product_id='.$product['product_id']}}">Delete</a>
                                            <a class="btn btn-sm btn-success" href="{{url('user/update-product').'?product_id='.$product['product_id']}}">Update</a>
                                        </td>
                                    </tr>

                                    @endforeach
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                  
                </div>

               
            </div>
            <!--row-->
            </div>
        <!--container-->
    </div>
    <!-- main area end -->
    @endsection
