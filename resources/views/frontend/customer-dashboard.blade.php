@extends('layouts/frontend/master')
@section('content')	
    
    <div class="main-area">
        <div class="container">
            <div class="row">
               
                <div class="col-sm-12">
                    <div class="panel panel-default mt20">
                        <div class="panel-heading">
                           
                                <h3 class="panel-title" style='padding-bottom:10px;'>User Dashboard
                                    <span class='pull-right'>
                                         
                                        <a class='btn btn-success btn-sm' href='{{url('user/profile').'/'.Auth::user()->id}}'><i class='fa fa-user'></i> User profile</a>
                                    </span>
                                </h3>
                                
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                
                                <!-- Highlights boxes -->
                                
                                <div class="col-sm-4">
                                    <div class='well well-lg text-warning'>
                                        <h2 class='text-center'><i class='fa fa-shopping-cart fa-2x'></i></h2>
                                        <p class='text-center'>Products Pending : {{$product_pending}}</p>
                                    </div>

                                </div>   
                                <div class="col-sm-4">
                                    
                                    <div class='well well-lg text-success'>
                                        <h2 class='text-center'><i class='fa fa-shopping-cart fa-2x'></i></h2>
                                        <p class='text-center'>Products Accepted : {{$product_accepted}}</p>
                                    </div>

                                </div>
                                <div class="col-sm-4">
                                    
                                    <div class='well well-lg text-danger'>
                                        <h2 class='text-center'><i class='fa fa-shopping-cart fa-2x'></i></h2>
                                        <p class='text-center'>Products Rejected : {{$product_rejected}}</p>
                                    </div>

                                </div>
                                
                                
                                
                                
                                <!-- Transaction Table -->
                                
                                <div style='padding:10px;'>
                                    
<!--                                    
                                    
                                    
                                    
                                    <!-- Feedbacks -->
                                @foreach($feedbacks as $feedback)
                                <div class='well well-sm'>
                                    <p style='font-style:italic; font-weight:bold;'>
                                        "{{$feedback['feedback']}}"
                                    </p>
                                    <p style='color:#ffd700'>
                                        <?php  $i = 1;?>
                                        <?php $rating = $feedback['rating'] ?>
                                        @while($i <= $rating)
                                        <i class='fa fa-star'></i>
                                        <?php $i++; ?>
                                        @endwhile 
                                        - {{$rating}}/5
                                    </p>
                                    <p>By: {{$feedback['name']}}</p>
                                    
                                </div>

                                @endforeach
                                    
                                </div>
                                
                            </div>
                        </div>
                    </div>
                  
                </div>

               
            </div>
            <!--row-->
            </div>
        <!--container-->
    </div>
    <!-- main area end -->
    @endsection
