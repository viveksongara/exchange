   @extends('layouts/frontend/master')
   @section('content')

    <!-- creative banner area start -->
    
    <!-- creative banner area end -->

    <!-- main area start -->
    <div class="main-area">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 nopadding-right">
                    <aside>
                        <div class="box_manufacrurer">
                            <div class="area-title">
                                <h3>Shop By</h3>
                            </div>
                            <ul class="box_list aside-padd">
                                <form method='post' enctype="multipart/form-data" action="{{url('show-categories-on-shoppage')}}">
                                    {{csrf_field()}}
                                
                                <li><span>Vendors</span></li>
                                <li>
                                    <input type="checkbox">Manufacturers name 01</li>
                                <li>
                                    <input type="checkbox">Manufacturers name 02</li>
                                <li>
                                    <input type="checkbox">Manufacturers name 03</li>
                                <li>
                                    <input type="checkbox">Manufacturers name 04</li>
                                <li>
                                    <input type="checkbox">Manufacturers name 05</li>
                                <li>
                                    <input type="checkbox">Manufacturers name 06</li>
                                <li>
                                    <input type="checkbox">Manufacturers name 07</li>
                                <li>
                                    <input type="checkbox">Manufacturers name 08</li>
                                <li>
                                    <input type="checkbox">Manufacturers name 09</li>
                                <li>
                                    <input type="checkbox">Manufacturers name 10</li>
                            </ul>
                            <input type='hidden' name='' value='$_GET['category_id']'> 
                            </form>
                        </div>
                    </aside>
                    <!--aside 1 end-->
                    <aside class="shop-filter aside-padd">
                        <h3 class="price-title">Price Filter</h3>
                        <div class="price_filter">
                            <form action="#" method="get">
                                <div id="slider-range"></div>
                                <input type="text" id="amount" readonly>
                                <input type="submit" value="Filter">
                            </form>
                        </div>
                    </aside>
                    <!--aside 2 end-->
                    <aside>
                        
                    </aside>
                    <!--aside 3 end-->
                    <aside>
                        <div class="tag-area">
                            
                        </div>
                    </aside>
                    <!--aside 4 end-->
                    <aside>
                        
                    </aside>
                    <!--aside 5 end-->
                </div>

                <!--col-md-3-->
                <div class="col-md-9 col-sm-9 nopadding-left">
                    <div class="ambit-key">
                        <div class="col-md-12 pt40">
                            <ol class="breadcrumb">
                                <li class="home"><a href="index.html" title="Go to Home Page">Home</a></li>
                                <li class="active">Shop</li>
                            </ol>
                            <div class="shop-banner mt20 mb20">
                                <a href="#"><img src="{{asset('assets/frontend')}}/img/clothing.jpg" alt=""></a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="shop-product-area">
                            <!-- area title start -->
                            <div class="col-sm-12">
                                <div class="area-title bdr">
                                    <h2>Clothing</h2>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <!-- area title end -->
                            <div class="short-area mt20">
                                <form id="sort_count" name="sort_count" method="post" action="#">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="sort-by">
                                            <label>Sort by:</label>
                                            <div class="select-sort-by">
                                                <select class="inputbox" name="order" id="order">
                                                    <option selected="selected">Name</option>
                                                    <option value="1">Predefined</option>
                                                    <option value="2">Price</option>
                                                    <option value="3">Date</option>
                                                    <option value="5">Rating</option>
                                                    <option value="6">Popular</option>
                                                </select>
                                                <a href="#"><i class="fa fa-long-arrow-up"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12 text-right">
                                        <div class="limiter">
                                            <label>Show:</label>
                                            <div class="select-limiter">
                                                <select class="inputbox" name="limit" id="limit">
                                                    <option value="99999">All</option>
                                                    <option value="5">5</option>
                                                    <option value="10">10</option>
                                                    <option value="15">15</option>
                                                    <option selected="selected" value="16">16</option>
                                                    <option value="20">20</option>
                                                    <option value="25">25</option>
                                                    <option value="50">50</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- product area start-->
                            <div class="row row-margin2">
                                <div class='col-sm-12'>
                                @if(Session::has('message'))
                                <div class='alert alert-success'>
                                    {{Session::get('message')}}
                                </div>
                                @endif
                                
                                @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                
                                </div>
                                @foreach($products['data'] as $product)
                                
                                <!-- single-product start -->
                                <div class="col-lg-3 col-md-4  col-sm-6  col-xs-12  col-padd">
                                    <div class="single-product">
                                        <div class="product-label">
                                            <div class="new"></div>
                                        </div>
                                        <div class="product-img">
                                            <a href="{{url('product-details').'?product_id='.$product['product_id']}}">
                                                <img class="primary-image" src="{{asset('assets/frontend/img/product').'/'.$product['product_image']}}" alt="" />
                                            </a>
                                        </div>
                                        <div class="product-content">
                                            <h2 class="product-name"><a href="#">{{$product['product_title']}}</a></h2>
                                            <div class="rating">
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <a href="#"><i class="fa fa-star-o"></i></a>
                                            </div>
                                            <div class="price-box">
                                                <span class="new-price">${{$product['price']}}</span>
                                            </div>
                                        </div>
                                        <div class="product-content2">
                                            <h2 class="product-name"><a href="#">Donec ac tempus</a></h2>
                                            <div class="rating">
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <a href="#"><i class="fa fa-star-o"></i></a>
                                            </div>
                                            <div class="price-box">
                                                <span class="new-price">${{$product['price']}}</span>
                                            </div>
                                            <div class="button-container">
                                                @if(Auth::check())
                                                <a title="Add to Cart" href="{{url('user/apply-product-for-exchange').'?product_id='.$product['product_id']}}" class="button cart_button">
                                                    <span>Apply</span>
                                                </a>

                                                @else
                                                
                                                <a title="Add to Cart" href="{{url('login')}}" class="button cart_button">
                                                    <span>Login to Apply</span>
                                                </a>

                                                @endif
                                                
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <!-- single-product end -->

                                @endforeach
                                
                               
                            </div>
                        </div>
                        <!--shop product area end-->
                        <div class="col-md-12">
                            
                                <ul class="pagination">
                                <li class="@if($products['prev_page_url'] == '') disabled @else @endif"><a  href="{{$products['prev_page_url']}}">Previous</a></li>
                                <?php
                                $last_page = $products['last_page'];
                                $i = 1;
                                ?>
                                @while($i <= $last_page)
                                <li class="@if($products['current_page'] == $i) disabled @else @endif"><a href="{{url('shop').'?page='.$i}}">{{$i}}</a></li>
                                <?php $i++; ?>
                                @endwhile
                                <li class="@if($products['next_page_url'] == '') disabled @else @endif"><a href="{{$products['next_page_url']}}">Next</a></li>
                            </ul>
                            
                            
                            
                        </div>
                        <!--brand crasoule-area-start-->
                        <div class="brand-crasoule-area featur-padd">
                            <!-- area title start -->
                            
                        </div>
                        <!--brand crasoule-area-end-->
                    </div>
                    <!--ambit-key-->
                </div>
                <!--col-md-9-->
            </div>
            <!--row-->
        </div>
        <!--container-->
</div>
    <!-- main area end -->
    
    @endsection

    