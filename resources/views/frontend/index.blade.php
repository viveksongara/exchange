@extends('layouts/frontend/master')
@section('content')	
    <!-- slider area start -->
    <div class="slider-area an-1 hm-1 clr">
        <!-- slider -->
        <div class="bend niceties preview-2">
            <div id="ensign-nivoslider" class="slides">
                <img src="{{asset('assets/frontend').'/'}}img/slider/slider1-bg.jpg" alt="" title="#slider-direction-1" />
                <img src="{{asset('assets/frontend').'/'}}img/slider/slider2-bg.jpg" alt="" title="#slider-direction-2" />
                <img src="{{asset('assets/frontend').'/'}}img/slider/slider3-bg.jpg" alt="" title="#slider-direction-3" />
            </div>
            <!-- direction 1 -->
            <div id="slider-direction-1" class="t-cn slider-direction">
                <div class="slider-content t-lfr lft-pr s-tb slider-1">
                    <div class="title-container s-tb-c title-compress">
                        <h2 class="title1">Baby's Clothing</h2>
                        <h3 class="title2">Save Up to</h3>
                        <h4 class="title2">50% Off</h4>
                        <a class="btn-title" href="single-product.html">Shop Now</a>
                    </div>
                </div>
                <!-- layer 1 -->
                <div class="layer-1-2">
                    <img src="{{asset('assets/frontend').'/'}}img/slider/slider1-for.png" alt="" />
                </div>
            </div>
            <!-- direction 2 -->
            <div id="slider-direction-2" class="slider-direction">
                <div class="slider-content t-lfr s-tb slider-2 lft-pr">
                    <div class="title-container s-tb-c">
                        <h2 class="title1">Home Appliances</h2>
                        <h3 class="title2">Save Up to</h3>
                        <h4 class="title2">50% Off</h4>
                        <a class="btn-title" href="single-product.html">Shop Now</a>
                    </div>
                </div>
                <!-- layer 1 -->
                <div class="layer-1-1">
                    <img src="{{asset('assets/frontend').'/'}}img/slider/slider2-for.png" alt="" />
                </div>
            </div>
            <!-- direction 3 -->
            <div id="slider-direction-3" class="slider-direction">
                <div class="slider-content t-lfr s-tb slider-3 lft-pr">
                    <div class="title-container s-tb-c">
                        <h2 class="title1">Birthday's Gift's</h2>
                        <h3 class="title2">Save Up To</h3>
                        <h4 class="title2">50% Off</h4>
                        <a class="btn-title" href="single-product.html">Shop Now</a>
                    </div>
                </div>
                <!-- layer 1 -->
                <div class="layer-1">
                    <img src="{{asset('assets/frontend').'/'}}img/slider/slider3-for.png" alt="" />
                </div>
            </div>
        </div>
        <!-- slider end-->
    </div>
    <!-- end home slider -->
    <!-- creative banner area start -->
    <div class="creative-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12">
                    <div class="banner-box-1 banner-inner">
                        <div class="banner-title">FREE SHIPPING</div>
                        <div class="banner-content">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="banner-box-2 banner-inner bannneer">
                        <div class="banner-title">30 DAYS MONEY BACK</div>
                        <div class="banner-content">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="banner-box-3 banner-inner">
                        <div class="banner-title">PAYMENT SECURED</div>
                        <div class="banner-content">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- creative banner area end -->
    <!-- main area start -->
    <div class="main-area">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 nopadding-right">
                    <aside>
                        <div class="left-category-menu">
                            <div class="left-product-cat">
                                <!--  Second search -->
                               <form id="" method="post"  action="{{url('search-result')}}">
                                     {{csrf_field()}}
                                    <select id="product_type" name="product_type" class="form-control">
                                        <option value="">Choose Product Type </option>
                                        <option value="looking">Looking</option>
                                        <option value="offering">Offering</option>
                                    </select>
                                    <select id="country" name="country" class="form-control">
                                        <option value="">Choose Country</option>
                                        @foreach($country as $country)
                                        <option @if(old('country_id') == $country['country_id']) selected @endif  value="{{$country['country_id']}}">{{$country['country_name']}}</option>
                                        @endforeach
                                    </select>
                                    <select id="state" name="state" class="form-control">
                                        <option value="">Choose State</option>
                                        @foreach($state as $state)
                                        <option @if(old('state_id') == $state['state_id']) selected @endif  value="{{$state['state_id']}}">{{$state['state_name']}}</option>
                                        @endforeach
                                    </select>
                                    <select id="city" name="city" class="form-control">
                                        <option value="">Choose City</option>
                                        @foreach($city as $city)
                                        <option @if(old('city_id') == $city['city_id']) selected @endif  value="{{$city['city_id']}}">{{$city['city_name']}}</option>
                                        @endforeach
                                     </select>
                                     <button class='btn btn-primary ' type='submit'><i class='fa fa-search'>Search</i></button>
                                                                         
                                  </form>
                                <!-- search end -->
                              
                                <hr>                              
                                
                                <div class="category-heading">
                                    <h2>category</h2>
                                </div>
                                <!-- category-menu-list start -->
                                <div class="category-menu-list">
                                    
                                    <ul>
                                        @foreach($categories as $category)
                                        
                                        @if(empty($category['subcategories']))
                                        
                                        <li>
                                            <a href="{{url('shop')}}"><img src="{{asset('assets/frontend').'/'}}img/catg-side/7.jpg" alt="" /><span>{{$category['category_name']}}</span></a>
                                        </li>
                                        
                                        @else
                                        
                                        <li>
                                            <a href="{{url('shop')}}"><img src="{{asset('assets/frontend').'/'}}img/catg-side/6.jpg" alt="" /><span>{{$category['category_name']}}</span><i class="fa fa-angle-right"></i></a>
                                            <!-- cat-left mega menu start -->
                                            <div class="cat-left-drop-menu subsm-drop">
                                                <div class="cat-left-drop-menu-left common0">
<!--                                                    <a class="menu-item-heading" href="shop.html">Jeans</a>-->
                                                    <ul>
                                                        
                                                        @foreach($category['subcategories'] as $cat)
                                                        <li><a href="{{url('shop')}}">{{$cat['subcategory_name']}}</a></li>
                                                        @endforeach
                                                        
                                                        
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- cat-left mega menu end -->
                                        </li>

                                        @endif

                                        @endforeach
                                        
                                        
                                        
                                    </ul>
                                </div>
                                <!-- category-menu-list end -->
                            </div>
                        </div>
                    </aside>
                    <!--aside 1 end-->
                    <aside>
                       
                    </aside>
                    <!--aside 2 end-->
                    <aside>
                        <div class="newsletter-area">
                            <div class="area-title">
                                <h3>Newsletter</h3>
                            </div>
                            <div class="aside-padd">
                                <div class="vina-newsletter">
                                    <form method="post" action="#">
                                        <div class="input-box">
                                            <label>Sign Up for Our Newsletter:</label>
                                            <input type="email" placeholder="Email" name="email">
                                        </div>
                                        <div class="input-box">
                                            <input type="submit" class="submit-btn" name="submit" value="Subscribe">
                                        </div>
                                    </form>
                                </div>
                                <div class="web-links">
                                    <ul>
                                        <li><a href="#" class="rss"><i class="fa fa-rss"></i>
                        </a></li>
                                        <li><a href="#" class="ldin"><i class="fa fa-linkedin"></i>
                        </a></li>
                                        <li><a href="#" class="face"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#" class="google"><i class="fa fa-google-plus"></i>
                        </a></li>
                                        <li><a href="#" class="twitter"><i class="fa fa-twitter"></i>
                        </a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </aside>
                    <!--aside 3 end-->
                    
                    <!--aside 4 end-->
                </div>
                <!--col-md-3-->

                <div class="col-md-9 col-sm-9 col-xs-12 nopadding-left">
                    <div class="ambit-key">
                        <!-- product section start -->
                        <div class="our-product-area top-pd featur-padd">
                            <!-- area title start -->
                            <div class="area-title">
                                <h2>Our products</h2>
                            </div>
                            <!-- area title end -->
                            <!-- our-product area start -->
                            <div class="features-tab">
                                <!-- Nav tabs -->
                                <ul class="nav">
                                    <li role="presentation" class="active"><a href="#home" data-toggle="tab"><i class="fa fa-list"></i>Bestsellers</a></li>
                                    
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <!--================================Best sellar================================-->
                                    <div role="tabpanel" class="tab-pane carousel-btn carousle-pagi fade in active" id="home">
                                        <div class="row row-margin">
                                            <div class="feature-carousel pt20 pb20">
                                                
                                                @foreach($products as $product)
                                                
                                                <!-- single-product start -->
                                <div class="col-lg-12  col-padd">
                                    <div class="single-product">
                                        <div class="product-label">
                                            <div class="new"></div>
                                        </div>
                                        <div class="product-img">
                                            <a href="{{url('product-details').'?product_id='.$product['product_id']}}">
                                                <img class="primary-image" src="{{asset('assets/frontend/img/product').'/'.$product['product_image']}}" alt="" />
                                            </a>
                                        </div>
                                        <div class="product-content">
                                            <h2 class="product-name"><a href="#">{{$product['product_title']}}</a></h2>
                                            <div class="rating">
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <a href="#"><i class="fa fa-star-o"></i></a>
                                            </div>
                                            <div class="price-box">
                                                <span class="new-price">Product Type:{{$product['product_type']}}</span>
                                            </div>
                                            <div class="price-box">
                                                <span class="new-price">Price:{{$product['price']}}</span>
                                            </div>
                                        </div>
                                        <div class="product-content2">
                                            <h2 class="product-name"><a href="#">{{$product['product_condition_name']}}</a></h2>
                                            <div class="rating">
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <a href="#"><i class="fa fa-star-o"></i></a>
                                            </div>
                                            <div class="price-box">
                                                <span class="new-price"></span>
                                            </div>
                                            <div class="button-container">
                                                @if(Auth::check())
                                                <a title="Add to Cart" href="{{url('user/apply-product-for-exchange').'?product_id='.$product['product_id']}}" class="button cart_button">
                                                    <span>Offer</span>
                                                </a>

                                                @else
                                                
                                                <a title="Add to Cart" href="{{url('login')}}" class="button cart_button">
                                                    <span>Login to Offer</span>
                                                </a>

                                                @endif
                                                
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <!-- single-product end -->


                                                @endforeach
                                                
                                               
                                            </div>
                                        </div>
                                    </div>
                                   
                                </div>
                            </div>
                            <!-- product section end -->
                        </div>
                        <!-- our-product-area end-->
<div role="tabpanel" class="tab-pane carousel-btn carousle-pagi fade in active" id="home">
                                        <div class="row row-margin">
                                            <div class="feature-carousel pt20 pb20">
                                                
                                                @foreach($products as $product)
                                                
                                                <!-- single-product start -->
                                <div class="col-lg-12  col-padd">
                                    <div class="single-product">
                                        <div class="product-label">
                                            <div class="new"></div>
                                        </div>
                                        <div class="product-img">
                                            <a href="{{url('product-details').'?product_id='.$product['product_id']}}">
                                                <img class="primary-image" src="{{asset('assets/frontend/img/product').'/'.$product['product_image']}}" alt="" />
                                            </a>
                                        </div>
                                        <div class="product-content">
                                            <h2 class="product-name"><a href="#">{{$product['product_title']}}</a></h2>
                                            <div class="rating">
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <a href="#"><i class="fa fa-star-o"></i></a>
                                            </div>
                                             <div class="price-box">
                                                <span class="new-price">Product Type:{{$product['product_type']}}</span>
                                            </div>
                                            <div class="price-box">
                                                <span class="new-price">Price:{{$product['price']}}</span>
                                            </div>
                                        </div>
                                        <div class="product-content2">
                                            <h2 class="product-name"><a href="#">Donec ac tempus</a></h2>
                                            <div class="rating">
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <a href="#"><i class="fa fa-star-o"></i></a>
                                            </div>
                                            <div class="price-box">
                                                <span class="new-price"></span>
                                            </div>
                                            <div class="button-container">
                                                @if(Auth::check())
                                                <a title="Add to Cart" href="{{url('user/apply-product-for-exchange').'?product_id='.$product['product_id']}}" class="button cart_button">
                                                    <span>Offer</span>
                                                </a>

                                                @else
                                                
                                                <a title="Add to Cart" href="{{url('login')}}" class="button cart_button">
                                                    <span>Login to Offer</span>
                                                </a>

                                                @endif
                                                
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <!-- single-product end -->


                                                @endforeach
                                                
                                               
                                            </div>
                                        </div>
                                    </div>
                        <!--advertise area start-->
                        <div class="advertise-area mt10">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="single-add vina-banner">
                                    <a href="#"><img src="{{asset('assets/frontend').'/'}}img/banner/banner-home-content1.png" alt="" /> </a>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="single-add vina-banner">
                                    <a href="#"><img src="{{asset('assets/frontend').'/'}}img/banner/banner-home-content2.png" alt="" /></a>
                                </div>
                            </div>
                        </div>
                        <!--advertise area end-->

                        <!--====== best offered area start===== -->
                        
                        <!-- best offered area end-->

                        <!--purchase progress area start-->
                        
                        <!--purchase progress area end-->

                        <!--==================== Recently Bought area start==================== -->
                        
                        <!--==================== Recently Bought area end==================== -->

                        <!--==================== Our Favourites area start==================== -->
                        
                        <!--==================== Our Favourites area end==================== -->

                        <!--advertise area2 start-->
                        
                        <!--advertise area2 end-->

                        <!--brand crasoule-area-start-->
                        <div class="brand-crasoule-area featur-padd">
                            <!-- area title start -->
                            <div class="area-title bdr mt20">
                                <h2>Brand & Clients</h2>
                            </div>
                            <!-- area title end -->
                            <div class="total-brand menu-carousel carousel-btn-2 carousel-btn mt10 mb20">
                                <div class="single-brand">
                                    <img src="{{asset('assets/frontend').'/'}}img/brand1.png" alt="">
                                </div>
                                <div class="single-brand">
                                    <img src="{{asset('assets/frontend').'/'}}img/brand2.png" alt="">
                                </div>
                                <div class="single-brand">
                                    <img src="{{asset('assets/frontend').'/'}}img/brand3.png" alt="">
                                </div>
                                <div class="single-brand">
                                    <img src="{{asset('assets/frontend').'/'}}img/brand4.png" alt="">
                                </div>
                                <div class="single-brand">
                                    <img src="{{asset('assets/frontend').'/'}}img/brand1.png" alt="">
                                </div>
                                <div class="single-brand">
                                    <img src="{{asset('assets/frontend').'/'}}img/brand2.png" alt="">
                                </div>
                                <div class="single-brand">
                                    <img src="{{asset('assets/frontend').'/'}}img/brand3.png" alt="">
                                </div>
                                <div class="single-brand">
                                    <img src="{{asset('assets/frontend').'/'}}img/brand4.png" alt="">
                                </div>
                            </div>
                        </div>
                        <!--brand crasoule-area-end-->
                    </div>
                    <!--ambit-key-->
                    
                </div>
                <!--col-md-9-->
            </div>
            <!--row-->
            </div>
        <!--container-->
    </div>
    <!-- main area end -->
    @endsection
