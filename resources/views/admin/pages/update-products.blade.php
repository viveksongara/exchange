@extends('layouts/frontend/master')
@section('content')
<?php $product = $products[0]; ?>
    
    <div class="main-area">
        <div class="container">
            <div class="row">
               
                <div class="col-sm-12 mt20">
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Post a product</h3>
                        </div>
                        <div class="panel-body">
                            @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            
                            @if(Session::has('message'))
                            <div class='alert alert-success'>
                                {{Session::get('message')}}
                            </div>
                            @endif
                            <form method='post' enctype="multipart/form-data" action="{{url('user/make-product-updated')}}">
                                {{csrf_field()}}
                                <div class="form-group">

                                    <select name="category_id" class="form-control">
                                        <option value="">Choose Category</option>
                                        
                                        @foreach($categories as $category)                                  
                                            
                                        <option 
                                        
                                            @if(old('category_id') == "")
                                                
                                                @if($category['category_id'] == $product['category_id']) selected @endif
                                            
                                            @else
                                                
                                                @if(old('category_id') == $category['category_id'])selected @endif
                                                
                                            @endif
                                            
                                        value="@if (old('category_id') == ''){{$category['category_id']}}@else{{$category['category_id']}}@endif">{{$category['category_name']}}</option>
                                        @endforeach

                                    </select>

                                </div>
                                
                                <div class="form-group">
                                    <label for="input">Product Title</label>
                                    <input name="product_title" value="@if (old('product_title') == ''){{$product['product_title']}}@else{{old('product_title')}}@endif" type="text" class="form-control" id="input" placeholder="Product Title">
                                </div>
                                
                                <!-- img, price , desc, status -->
                                
                                <div class="form-group">
                                    <label for="input">Product Image</label>
                                    <input name="product_image" value="{{old('product_img')}}" type="file" class="" id="input">
                                </div>
                                
                                <div class="form-group">
                                    <label for="input">Product Price</label>
                                    <input name="product_price" value="@if (old('product_price') == ''){{$product['price']}}@else{{old('product_price')}}@endif" type="text" class="form-control" id="input" placeholder="Product Price">
                                </div>
                                
                                <div class="form-group">
                                    <label for="input">Product Description</label>
                                    <textarea name="product_description" class="form-control" rows="3">@if (old('product_title') == ''){{$product['product_description']}}@else{{old('product_description')}}@endif</textarea>
                                </div>
                                
                                <input type='hidden' name='product_id' value='{{$product['product_id']}}'>
                                <input class="btn btn-success" type="submit" value="Post Product">
                            </form>
                        </div>
                    </div>
                    
                  
                </div>

               
            </div>
            <!--row-->
            </div>
        <!--container-->
    </div>
    <!-- main area end -->
    @endsection
