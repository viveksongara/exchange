

@extends('layouts/admin/master')
@section('mainContent')
 


    

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content">
     
        <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"Read categories</h3>
                </div>
                <div class="panel-body">
                     @if(Session::has('message'))
                <div class='alert alert-success'>
                    {{Session::get('message')}}
                </div>
                @endif
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>user name</th>
                                 <th>email</th>
                                 
                                   <th>Phone</th>
                                   <th>	Company</th>
                                    
                                    
                                <th class="text-center">#</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                                @foreach($users as $user)
                                <tr>
                                    <td>{{$user['name']}}</td>
                                       
                                    
                                     <td>{{$user['email']}}</td>
                                      
                                      <td>{{$user['phone']}}</td>
                                      
                                      <td>{{$user['company']}}</td>
                                      <td><img style="max-width:80px;" class="img img-responsive" src="{{asset('assets/pics/profile_picture').'/'.$user['profile_picture']}}"></td>
                                      
                                      <td><a  class="btn btn-danger btn-sm"href="{{url('delete-users').'?id='.$user['id']}}">Delete</a></td>
                                    
                                </tr>
                                @endforeach
                          
                        </tbody>
                    </table>
                </div>
            </div>
        </div>    
    </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @endsection

----------------------------------