

@extends('layouts/admin/master')
@section('mainContent')


    

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content">
     <div class="row">
       
        <div class="col-sm-12">
            <div class="panel panel-default">
                
                <div class="panel-heading">
                    <h3 class="panel-title">Create category</h3>
                </div>
                <div class="panel-body">
                    {{-- yahain pe lara_form_error likhna padta h  --}}
                    
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    
                    @if(Session::has('message'))
                <div class='alert alert-success'>
                    {{Session::get('message')}}
                </div>
                @endif
                    <form method='post' enctype="multipart/form-data" action="{{url('admin/creating-subcategory')}}">
                        {{csrf_field()}}
                        
                        
                        <div class="form-group">

                                    <select id="categories" name="category_id" class="form-control">
                                        <option value="">Choose Category</option>
                                        @foreach($categories as $category)
                                        <option @if(old('category_id') == $category['category_id']) selected @endif value="{{$category['category_id']}}">{{$category['category_name']}}</option>
                                        @endforeach

                                    </select>

                                </div>
                        
                        <div class="form-group">
                            <label for="input">Enter subcategory</label>
                            <input name="subcategory_name" value="{{old('subcategory_name')}}" type="text" class="form-control" id="subcategory_name" placeholder="Text">
                        </div>
                        
                        
                        <input class="btn btn-success" type="submit" value="Submit">
                    </form>
                </div>
            </div>
        </div>    
    </div>
        

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @endsection
  

