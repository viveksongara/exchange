

@extends('layouts/admin/master')
@section('mainContent')
 


    

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content">
     
        <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"Read categories</h3>
                </div>
                <div class="panel-body">
                     @if(Session::has('message'))
                <div class='alert alert-success'>
                    {{Session::get('message')}}
                </div>
                @endif
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>category name</th>
                                <th class="text-center">#</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                                @foreach($subcategories as $subcategory)
                                <tr>
                                    <td>{{$subcategory['subcategory_name']}}</td>
                                    <td><a  class="btn btn-danger btn-sm"href="{{url('admin/delete-subcategory').'?sub_category_id='.$subcategory['sub_category_id']}}">Delete</a></td>
                                </tr>
                                @endforeach
                          
                        </tbody>
                    </table>
                </div>
            </div>
        </div>    
    </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @endsection


