

@extends('layouts/admin/master')
@section('mainContent')
 


    

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content">
     <div class="row">
       
        <div class="col-sm-12">
            <div class="panel panel-default">
                
                <div class="panel-heading">
                    <h3 class="panel-title">Create Users</h3>
                </div>
                <div class="panel-body">
                    {{-- yahain pe lara_form_error likhna padta h  --}}
                    
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    
                    @if(Session::has('message'))
                <div class='alert alert-success'>
                    {{Session::get('message')}}
                </div>
                @endif
                    <form method='post' enctype="multipart/form-data" action="{{url('add-users')}}">
                        {{csrf_field()}}
                        
                        
                        <div class="form-group">
                            <label for="input">user name</label>
                            <input name="name" value="{{old('name')}}" type="text" class="form-control" id="name" placeholder="Enter user name">
                        </div>
                        <!-- password field -->
                        <div class="form-group">
                            <label for="input">password</label>
                            <input  name="password"value="{{old('password')}}" type="text" class="form-control" id="password" placeholder="Enetr user password">
                        </div>
                        <!-- email -->
                        <div class="form-group">
                            <label for="input">email</label>
                            <input  name="email"value="{{old('email')}}" type="text" class="form-control" id="email" placeholder="Enter email">
                        </div>    
                        <!-- country field -->
                        <div class="form-group">
                            <label for="input">Phone</label>
                            <input  name="phone"value="{{old('phone')}}" type="text" class="form-control" id="phone" placeholder="Enter country">
                        </div>
                        <!-- state field -->
                        <div class="form-group">
                            <label for="input">Company</label>
                            <input  name="company"value="{{old('company')}}" type="text" class="form-control" id="company" placeholder="Enter state">
                        </div>
                        <!-- city field -->
                        <div class="form-group">
                            <label for="input">Postcode</label>
                            <input  name="postcode"value="{{old('postcode')}}" type="text" class="form-control" id="postcode" placeholder="Enter city">
                        </div>
                        <!-- can accept payment field -->
                        <div class="form-group">
                            <label for="input">Payment status</label>
                            <input name="can_accept_payment" value="{{old('can_accept_payment')}}" type="text" class="form-control" id="can_accept_payment" placeholder="Enter payment status">
                        </div>
                        <input class="btn btn-success" type="submit" value="Submit">
                    </form>
                </div>
            </div>
        </div>    
    </div>
        

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @endsection