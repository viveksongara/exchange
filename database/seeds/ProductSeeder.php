<?php

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    
        
        DB::table('products')->insert([
            'category_id' => '1',
            'sub_category_id' => '1',
            'product_condition_id' => '1',
            'country_id' => '1',
            'state_id' => '1',
            'city_id' => '1',
            'postcode' => '777777',
            'vendor_id'   => '2',
            'product_title' => 'Product One',
            'product_image' => 'product.jpg',
            'price'         => '100',
            'product_description' => 'Product Description',
            'product_status' => '1',
            'product_type' => 'offering'
        ]);
        
        DB::table('products')->insert([
            'category_id' => '1',
            'sub_category_id' => '1',
            'product_condition_id' => '1',
            'country_id' => '1',
            'state_id' => '1',
            'city_id' => '1',
            'postcode' => '777777',
            'vendor_id'   => '2',
            'product_title' => 'Product Two',
            'product_image' => 'product.jpg',
            'price'         => '100',
            'product_description' => 'Product Description',
            'product_status' => '1',
            'product_type' => 'offering'
        ]);
        
        DB::table('products')->insert([
            'category_id' => '1',
            'sub_category_id' => '1',
            'product_condition_id' => '1',
            'country_id' => '1',
            'state_id' => '1',
            'city_id' => '1',
            'postcode' => '777777',
            'vendor_id'   => '2',
            'product_title' => 'Product Three',
            'product_image' => 'product.jpg',
            'price'         => '100',
            'product_description' => 'Product Description',
            'product_status' => '1',
            'product_type' => 'offering'
        ]);
        
        DB::table('products')->insert([
            'category_id' => '1',
            'sub_category_id' => '1',
            'product_condition_id' => '1',
            'country_id' => '1',
            'state_id' => '1',
            'city_id' => '1',
            'postcode' => '777777',
            'vendor_id'   => '2',
            'product_title' => 'Product Four',
            'product_image' => 'product.jpg',
            'price'         => '100',
            'product_description' => 'Product Description',
            'product_status' => '1',
            'product_type' => 'offering'
        ]);
        
        DB::table('products')->insert([
            'category_id' => '1',
            'sub_category_id' => '1',
            'product_condition_id' => '1',
            'country_id' => '1',
            'state_id' => '1',
            'city_id' => '1',
            'postcode' => '777777',
            'vendor_id'   => '2',
            'product_title' => 'Product Five',
            'product_image' => 'product.jpg',
            'price'         => '100',
            'product_description' => 'Product Description',
            'product_status' => '1',
            'product_type' => 'offering'
        ]);
        
        DB::table('products')->insert([
            'category_id' => '1',
            'sub_category_id' => '1',
            'product_condition_id' => '1',
            'country_id' => '1',
            'state_id' => '1',
            'city_id' => '1',
            'postcode' => '777777',
            'vendor_id'   => '2',
            'product_title' => 'Product Six',
            'product_image' => 'product.jpg',
            'price'         => '100',
            'product_description' => 'Product Description',
            'product_status' => '1',
            'product_type' => 'offering'
        ]);
    }
}
