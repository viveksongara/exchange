<?php

use Illuminate\Database\Seeder;

class SubcategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('subcategories')->insert([
            'sub_category_id' => '1',
            'category_id'   => '2',
            'subcategory_name' => 'Armani',
            
        ]);
    }
}
