<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        
        DB::table('users')->insert([
            'name'    => 'Priyank Bhargava',
            'email'   => 'admin@suryawebtech.com',
            'phone'   => '9407183852',
            'role_id' => '2',
            'password' => bcrypt('shriyank1986'),
            'activation_status' => '1',
            'profile_picture' => 'default-profile-pic.jpg',
            'firstname'       => 'Priyank',
            'lastname'        => 'Bhargava',
            'company'         => 'Suryawebtech'
        ]);
        
        DB::table('users')->insert([
            'name'    => 'Surbhi Bhargava',
            'email'   => 'surbhi@suryawebtech.com',
            'phone'   => '9407183852',
            'role_id' => '1',
            'password' => bcrypt('shriyank1986'),
            'activation_status' => '1',
            'profile_picture' => 'default-profile-pic.jpg',
            'firstname'       => 'Surbhi',
            'lastname'        => 'Bhargava',
            'company'         => 'Suryawebtech'
        ]);
        
        DB::table('users')->insert([
            'name'    => 'Swati Joshi',
            'email'   => 'swati@suryawebtech.com',
            'phone'   => '9407183852',
            'role_id' => '1',
            'password' => bcrypt('shriyank1986'),
            'activation_status' => '1',
            'profile_picture' => 'default-profile-pic.jpg',
            'firstname'       => 'Swati',
            'lastname'        => 'Joshi',
            'company'         => 'Suryawebtech'
        ]);
    }
}
