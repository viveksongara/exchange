<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class FeedbackSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('feedbacks')->insert([
            'given_by'    => '3',
            'received_by' => '2',
            'feedback'    => 'Very nice product',
            'rating'      => '5'
        ]);
    }
}
