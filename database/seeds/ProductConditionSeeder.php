<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ProductConditionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * 	Band new
	New
	Used but looks new
	Used
	For parts
	Not working
	Damaged
	Fair

         */
        $conditions = array(
            'Brand New',
            'New',
            'Used but looks new',
            'Used',
            'For parts',
            'Not working',
            'Damaged',
            'Fair'
        );
        
        foreach ($conditions as $condition) {
            DB::table('product_conditions')->insert([
                'product_condition_name' => $condition 
            ]);
        }
        
    }
}
