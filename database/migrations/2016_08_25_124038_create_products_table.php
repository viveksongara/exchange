<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('product_id');
            $table->integer('category_id'); 
            $table->integer('country_id');
            $table->integer('state_id');
            $table->integer('city_id');
            $table->string('postcode');
            $table->enum('product_type', ['looking','offering']);
            $table->integer('product_condition_id');
            $table->integer('vendor_id');
            $table->string('product_title');
            $table->string('product_image');
            $table->string('price');
            $table->integer('sub_category_id');
            $table->string('product_description');
            $table->string('product_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
