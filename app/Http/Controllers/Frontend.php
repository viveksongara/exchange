<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

use App\Product;

use App\Product_condition;

use App\Category;

use App\Feedback;

use App\Order;

use App\Exchange;

use App\Subcategory;

use App\Country;

use App\State;

use App\City;

//use App\Country;

use Illuminate\Support\Facades\Mail;

use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\Session;

use Illuminate\Support\Facades\Auth;

class Frontend extends Controller
{
    public function index() {
        
        $data['products'] = Product::
                            join('categories','categories.category_id','=','products.category_id')
                           ->join('product_conditions','product_conditions.product_condition_id','=','products.product_condition_id')
                            ->take(5)
                            ->get()
                            ->toArray();
        
        $categories  = Category::get()->toArray();
        
        // put subcategories in category array
        $i = 0;
        foreach ($categories as $category) {
        
        $categories[$i]['subcategories'] = $this->get_sub_category_by_id($category['category_id']);
        $i++;
        }
        
        $data['categories'] = $categories;
        //select country
        $country['country'] = Country::select('country_name','country_id')
                            ->get()
                            ->toArray();
        //select state
        $state['state'] = State::select('state_name','state_id')
                            ->get()
                            ->toArray();
        //select city
        $city['city'] = City::select('city_name','city_id')
                            ->get()
                            ->toArray();
        
  
        return view('frontend/index',$data)->with($country)->with($state)->with($city);
    }
    
    private function get_sub_category_by_id($category_id) {
       
        $sub_categories = Subcategory::where('category_id','=',$category_id)->get()->toArray();
        
        if (count($sub_categories) > 0) {
            return $sub_categories;
        }else{
            $blank_array = array();
            return $blank_array;
        }
        
        
        
    }   
    
    
    public function signup(){
        
        $data['countries'] = Country::get()->toArray();
        return view('frontend.signup',$data);
    }
    
    public function user_registration(Request $request) {
        
        
        
        $this->validate($request,[
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'phone' => 'required|numeric',
            'country_id' => 'required|numeric',
            'state_id' => 'required|numeric',
            'city_id' => 'required|numeric'
            
        ]);
        
        
        $user                      = new User();
        
        $user->name                = $request->name;
        $user->email               = $request->email;
        $user->password            = bcrypt($request->password);  
        $user->phone    = $request->phone;
        $user->country_id = $request->country_id;
        $user->state_id = $request->state_id;
        $user->city_id = $request->city_id;
        
        $confirmation_code         = time().rand(1, 10000);
        $user->confirmation_code   = $confirmation_code;
        $user->activation_status   = 0;
        $user->save();
        
        
        
        // Send the email confirmation email to user to verify
        
        $email_data = array(
            'email'             => $request->email,
            'confirmation_code' => $confirmation_code
        );
        
        $data['countries'] = Country::get()->toArray();
        
        Mail::send('frontend.email.register', $email_data, function($message) use ($request) {     

        
        $message->replyTo($request->email,$request->first_name.' '.$request->last_name);
        $message->subject('Registration Successful');
        $message->to($request->email);
        });
        
        return redirect('user-registration-success')->with('message','Thank you, Check your mail to activate your account.');
        
        
        
    }
    
    public function confirm_user(Request $request) {
        
        $this->validate($request,[
           'email' => 'required|email',
           'confirmation_code' => 'required|numeric'
        ]);
        
        $count = User::where('email','=',$request->email)
                 ->where('confirmation_code','=',$request->confirmation_code)
                 ->count();
        
         if ($count > 0) {
             
             $user = User::where('email','=',$request->email)
                     ->where('confirmation_code','=',$request->confirmation_code);
             
             $update = array(
                 'activation_status' => '1',
                 
             );
             $user->update($update);
             
                 
             
             
             return redirect('user-registration-success')->with('message','Thank you, you are successfuly registered.');
             
         }
        
    }
    
    public function logout() {
        
        \Illuminate\Support\Facades\Auth::logout();
        
        return redirect('/');
        
    }
	
    public function excel_test(){
		Excel::create('swatijoshi', function($excel) {

			$excel->sheet('iloveyou', function($sheet) {

				$sheet->fromArray(array(
					array('data1', 'data2'),
					array('data3', 'data4')
				));

			});

		})->export('xls');
	}
        
    public function shop(Request $request){
        
        $data['products'] = Product::join('users','products.vendor_id','=','users.id')
                             ->join('categories','categories.category_id','=','products.category_id')
                             ->paginate(4)
                             ->toArray();
//        $data['products_'] = Product::join('product_conditions','products.product_condition_id','=','product_conditions.product_condition_id')
//                ->get()->toArray();
        
         if (Auth::check()) {
             $data['items_to_offer'] = Product::where('vendor_id','=',Auth::user()->id)
                                       ->get()->toArray();
         }
        
        return view('frontend.shop',$data);
        
    }
        
    /////////////////////////////// Customer ////////////////////////////////////////
    
    public function user_dashboard(Request $request) {
        
        $data['product_pending'] = Exchange::where('user_id','=',$request->user()->id)
                                   ->where('is_accepted','=','0')->count();
        
        $data['product_accepted'] = Exchange::where('user_id','=',$request->user()->id)
                                   ->where('is_accepted','=','1')->count();
        
        $data['product_rejected'] = Exchange::where('user_id','=',$request->user()->id)
                                   ->where('is_accepted','=','2')->count();
        
//        $data['orders'] = Order::where('users.id','=',$request->user()->id)
//                                 ->join('users','users.id','=','orders.user_id')
//                                 //->join('products','orders.product_id','=','products.product_id')
//                                //  ->where('product_type','=','offering')
//                                // ->orderBy('orders.created_at','desc')
//                                // ->paginate(10)
//                                ->get()
//                                 ->toArray();
        
        $data['feedbacks'] = Feedback::where('received_by','=',$request->user()->id)
                             ->join('users','users.id','=','feedbacks.given_by')
                             ->get()->toArray();
       
            
                                 
        
        return view('frontend.customer-dashboard',$data);
        
    }
    
    public function products_applied_for_exchange(Request $request) {
        
        $data['products'] = Exchange::where('user_id','=',$request->user()->id)
                            ->join('products','products.product_id','=','exchanges.product_id')
                            ->join('users','products.vendor_id','=','users.id')
                            ->orderBy('offer_date','desc')
                            ->get()->toArray();
        
        return view('frontend.products_applied_for_exchange',$data);
        
        
    }
    
     public function feedback(Request $request) {  
        
        $this->validate($request,[
            'exchange_id' => 'required|numeric',
            'user_id'     => 'required|numeric'
        ]);
        
        $given_by = $request->user_id;
        $receiver_details = Exchange::where('exchange_id','=',$request->exchange_id)
                            ->join('products','exchanges.product_id','=','products.product_id')                            
                            ->get()->toArray();
        
        $received_by = $receiver_details[0]['vendor_id'];
       
        $data['exchange_id'] = $request->exchange_id;
        $data['user_id'] =     $request->user_id;
        $data['given_by'] =    $given_by;
        $data['received_by'] = $received_by;

        return view('frontend.feedback',$data);
        
    }
    
     public function make_feedback(Request $request) {  
       
         $this->validate($request, [
             'given_by' => 'required|numeric',
             'received_by' => 'required|numeric',
             'feedback'    => 'required',
             'rating' => 'required|numeric'
         ]);
         
         $feedback = new Feedback();
         $feedback->given_by = $request->given_by;
         $feedback->received_by = $request->received_by;
         $feedback->feedback = $request->feedback;
         $feedback->rating = $request->rating;
         $feedback->save();
  
       return redirect()->back()->with('message','Profile is successfully updated');
       
    }
    
    
    // customer application for exachange of product 
    
    public function apply_product_for_exchange(Request $request){
        
        $this->validate($request, [
        'product_id' => 'required|numeric',
        'product_two_id' => 'required|numeric'
        ]);
        
        $product = new Exchange();
        $product->product_id = $request->product_id;
        $product->product_two_id = $request->product_two_id;
        $product->user_id    = $request->user()->id;
        $product->offer_date = strftime('%Y-%m-%d',time());
        $product->offer_time = strftime('%H:%M:%S',time());
        $product->save();
        
        return redirect()->back()->with('message', 'You have successfully applied for exachage, we will notify you by email about status');
        
        
        
    }
    
    public function transaction_history(Request $request) {
        
        $data['orders'] = Order::where('user_id','=',$request->user()->id)
                         ->join('orders','products.product_id','=','orders.product_id')
                         ->get()->toArray();
        
        return view('frontend.transactions',$data);
        
    }
    
    public function make_payment_to_admin(Request $request) {
        $data['exchanges'] =  Exchange::where('exchange_id','=',$request->exchange_id)
                            ->join('products','products.product_id','=','exchanges.product_id')
                            ->join('users','users.id','=','products.vendor_id')
                            ->get()
                            ->toArray();
        return view('frontend.make-pay-to-admin',$data);
        
    }
    
    public function show_vendor_details(Request $request) {
        
    }
    
    /////////////////////////// Vendor  Functions //////////////////////////////////
    
    public function products(Request $request){
        
        $data['products'] = Product::where('vendor_id','=',$request->user()->id)
                            ->join('categories','categories.category_id','=','products.category_id')
                            
                            ->get()->toArray();
           
           
        
        return view('frontend.products',$data);
    }
    
    public function items_to_swap(Request $request) {
        
        $data['products'] = Product::where('vendor_id','=',$request->user()->id)
                            ->where('product_type','=','looking')
                            ->join('categories','categories.category_id','=','products.category_id')
                            ->get()->toArray();
        
        return view('frontend.items_to_swap',$data);
        
    }
    
    public function items_to_exchange(Request $request){
        
        $data['products'] = Product::where('vendor_id','=',$request->user()->id)
                            ->where('product_type','=','offering')
                            ->join('categories','categories.category_id','=','products.category_id')
                            ->get()->toArray();
        
        return view('frontend.items_to_exchange',$data);
        
    }
    
    public function post_product(Request $request) {       
        
        
        $data['categories'] = Category::get()->toArray();
        
        $data['product_conditions'] = Product_condition::get()->toArray();
         
        $data['subcategories'] = Subcategory::where('category_id','=',$request->category_id)->get()->toArray();
        
        $data['countries'] = $this->get_all_countries();
        
        return view('frontend/post-product',$data);

    }
    
    public function make_product_posted(Request $request) {
        
        $this->validate($request, [
        'category_id' => 'required|numeric',
        'sub_category_id' => 'required|numeric',
        
        'country_id' => 'required|numeric',
        'state_id'   => 'required|numeric',
        'city_id'    => 'required|numeric',
        //'price'    => 'required',    
        'product_title'    => 'required',
        //'product_image' => 'required|mimes:jpeg,bmp,png',
        'product_type' => 'required|in:looking,offering',
        
       // 'product_condition_id' => 'required|numeric',
        
        ]);
        
        $product = new Product();
        
        $product->category_id = $request->category_id;
        $product->sub_category_id = $request->sub_category_id;
        $product->product_type = $request->product_type;
        $product->country_id = $request->country_id;
        $product->state_id = $request->state_id;
        $product->product_title = $request->product_title;
        $product->city_id = $request->city_id;
        $product->vendor_id   = $request->user()->id;
        $product->product_title = $request->product_title;
        $product->postcode        = $request->postcode;
         $product->product_condition_id = $request->product_condition_id;
         //$product->price = $request->price;
         
       // $product->product_description   = $request->product_description;
        
        if (Input::hasFile('product_image')) {
            $file = Input::file('product_image');
            $file = $file->move(public_path() . '/assets/frontend/img/product', rand(10000, 99999) . time() . '.' . $file->getClientOriginalExtension());
           
            /**********************Note to remember **************************
             * 
             * When in windows switch / to \\
             * when in cpanel or mac switch \\ to / 
             */
            $name = explode('\\', $file->getRealPath());
            $product->product_image = end($name);
            
        }
       
        $product->save();
        
        return redirect()->back()->with('message', 'Product is successfully posted');
        
        
        
        
    }
    
    // To show update product page
    public function update_product(Request $request) {
        
        
        $this->validate($request, [
           'product_id' => 'required|numeric'
        ]); 
        
        $data['categories'] = Category::get()->toArray();
        
        $data['products'] = Product::where('product_id','=',$request->product_id)
                            ->get()
                            ->toArray();
        
        return view('frontend.update-products',$data);
       
        
        
    }    
    
    // Make the product actually updated
    public function make_product_updated(Request $request) {
        
        $this->validate($request, [
        'category_id' => 'required|numeric',
        'product_title' => 'required',
        'country_id' => 'required|numeric',
        'state_id'   => 'required|numeric',
        'city_id'    => 'required|numeric',
        'product_type' => 'required|in:looking,offering',
        //'product_image' => 'required|mimes:jpeg,bmp,png',
        'product_price' => 'required|numeric',
        
        'product_description' => 'required'
        ]);
        
        $product = Product::find($request->product_id);
        $product->category_id = $request->category_id;
        $product->vendor_id   = $request->user()->id;
        $product->product_type = $request->product_type;
        $product->country_id = $request->country_id;
        $product->state_id = $request->state_id;
        $product->city_id = $request->city_id;
        $product->product_title = $request->product_title;
        $product->price         = $request->product_price;
        $product->product_condition_id = $request->product_condition_id;
        $product->product_description   = $request->product_description;
        
        if (Input::hasFile('product_image')) {
            $file = Input::file('product_image');
            $file = $file->move(public_path() . '/assets/frontend/img/product', rand(10000, 99999) . time() . '.' . $file->getClientOriginalExtension());
            $name = explode('\\', $file->getRealPath());
            $product->product_image = end($name);
            
        }
        
        $product->save();
        
        return redirect()->back()->with('message', 'Product is successfully updated');
        
        
    }
    
    // Delete the product
    public function delete_product(Request $request) {
        
        $this->validate($request, [
        'product_id' => 'required|numeric'
        ]);
        
        Product::where('product_id','=',$request->product_id)->delete();
        
        return redirect()->back()->with('message', 'Product is successfully deleted');
        
    }
    
    public function vendor_dashboard(Request $request) {
        
        $data['product_pending'] = Exchange::where('vendor_id','=',$request->user()->id)
                                   ->join('products','products.product_id','=','exchanges.product_id')
                                   ->where('is_accepted','=','0')->count();
        
        $data['product_accepted'] = Exchange::where('vendor_id','=',$request->user()->id)
                                   ->join('products','products.product_id','=','exchanges.product_id')
                                   ->where('is_accepted','=','1')->count();
        
        $data['product_rejected'] = Exchange::where('vendor_id','=',$request->user()->id)
                                   ->join('products','products.product_id','=','exchanges.product_id')
                                   ->where('is_accepted','=','2')->count();
        
        
        
        return view('frontend.vendor-dashboard',$data);
        
    }       
    
    public function exchange_requests_to_vendor(Request $request) {
        
        $products            = Exchange::where('vendor_id','=',$request->user()->id)
                            ->join('products','products.product_id','=','exchanges.product_id')
                            ->join('users','users.id','=','exchanges.user_id')
                            ->get()
                            ->toArray();
        
        // product ki table ko 2 baar nahi jod sakti toh recursive loop lagana pada
        $i = 0;
        foreach($products as $product){
           $products[$i]['product_two_title'] =  $this->get_product_title_by_id($product['product_two_id']);
           $i++;
           
        }
        
        
        
        $data['products'] = $products;
        
        return view('frontend.exchange-requests',$data);
        
    } 
    
    private function get_product_title_by_id($product_id){
        
        $products = Product::find($product_id);
        return $products['product_title'];
        
    }
        
    public function approve_exchange(Request $request) {
        
        $this->validate($request, [
           'exchange_id' => 'required|numeric'
        ]); 
        
        $exchange = Exchange::find($request->exchange_id);
        $exchange->is_accepted = '1';
        $exchange->save();
        
        // Get the product id of last approved exchange
        
        $exchange_id = $exchange->exchange_id;
        
        $exchange_details = Exchange::where('exchange_id','=',$exchange_id)
                            ->join('products','products.product_id','=','exchanges.product_id')
                            ->join('users','users.id','=','products.vendor_id')
                            ->get()
                            ->toArray();
        
        $vendor_name = $exchange_details[0]['name'];
        
        $data['user_details'] = Exchange::where('exchange_id','=',$exchange_id)
                               ->join('users','users.id','=','exchanges.user_id')
                                ->get()
                                ->toArray();
        
        
        
        $email_data = array(
            'vendor_name' => $vendor_name,
            'product_title' => $exchange_details[0]['product_title'],
            'exchange_id'  => $exchange_details[0]['exchange_id']
        );


        Mail::send('frontend.email.customer-notification', $email_data, function($message) use ($data) { 

            
            $message->replyTo('swati@suryawebtech.com', 'Company' . ' ' . 'Name');
            $message->subject('Congrats,Product Approved By Vendor!!');
            //$message->to($data['user_details'][0]['email']);
            $message->to('priyankbhargava2008@gmail.com');
        });
        
        return redirect()->back()->with('message', 'Exchange is successfully approved');
        
        
    }
    
    public function reject_exchange(Request $request) {
        
        $this->validate($request, [
           'exchange_id' => 'required|numeric'
        ]); 
        
        $exchange = Exchange::find($request->exchange_id);
        $exchange->is_accepted = '2';
        $exchange->save();
        
        return redirect()->back()->with('message', 'Exchange is successfully rejected');
        
        
    }
    
    
    
    // Reminder : Feedback will also be shown here 
    
    ////////////////////////// to show categories///////////////////////
    
   
    
    /////////////////////// to show vendors on shop page/////////////////
    
    public function show_vendors_on_shoppage() {
        
        $data['users'] = User::where('role_id','=','1')->get()->toArray();
        
        
        return view('frontend.shop',$data);
        
}

      //// to show category on search///////////
      
    public function search(Request $request) {
    
        //$postcode = $request->postcode;
        //$category_id = $request->category_id;
        //$product_title = $request->product_title;
        //$data['products'] = Product::join('users', 'users.id', '=', 'products.vendor_id')
        //->join('categories', 'categories.category_id', '=', 'products.category_id')
        //->where
        //([
        //['product_title', 'LIKE','%'.$product_title.'%'],
        //['products.postcode', '=', $postcode],
        //['categories.category_id', '=', $category_id],
        //])->get()->toArray();
     
      //second search-> ye check krega yadi second search se value aa rhi he to ye value pass ho 
        $product_type = $request->product_type;
        if($product_type!=Null)
         {
        $product_type = $request->product_type;
        $country_id = $request->country;
        $state_id = $request->state;
        $city_id = $request->city;
        $data['products'] = Product::select('*')
                ->where
               ([
                ['product_type', 'LIKE','%'.$product_type.'%'],
                ['country_id', '=', $country_id],
                ['state_id', '=', $state_id],
                ['city_id', '=', $city_id],   
                ])->get()->toArray();
       
         return view('frontend/search-result',$data);      
         }
        
        
        
      
        
    }
    


   // to show sub categories on post an item page
    
    
    public function get_subcategory_by_id(Request $request){
        $data['subcategories'] = Subcategory::where('category_id','=',$request->category_id)
                ->get()
                ->toJson();
        
        return $data['subcategories'];
    }
    
    ///////////////////////////////// Locations ///////////////////////////////////////////
    
    
    public function get_all_countries() {
        
        $data['countries'] = Country::get()->toArray();
        
        return $data['countries'];
        
    }
    
    public function get_states_by_country_id(Request $request) {
        
        $data['states'] = State::where('country_id','=',$request->country_id)->get()->toJson();
        
        return $data['states'];
    }
    
    public function get_cities_by_state_id(Request $request) {
        
        $data['cities'] = City::where('state_id','=',$request->state_id)->get()->toJson();
        
        return $data['cities'];
        
    }
    /////////////////////////////// single pages////////////////
    public function terms() {
       
        return view('frontend.terms');
        
    }
    
    public function contact() {
       
        return view('frontend.terms');
        
    }
    
    public function returns() {
       
        return view('frontend.terms');
        
    }
    
    public function services() {
       
        return view('frontend.terms');
        
    }
    
    public function special() {
       
        return view('frontend.terms');
        
    }
    
    public function page1() {
       
        return view('frontend.terms');
        
    }
    
    public function update_profile(Request $request) {
        
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            
        ]);
        
        $update = array();
        $update['name'] = $request->name;
        $update['phone'] = $request->phone;
        if ($request->password != '') {
            $update['password'] = bcrypt($request->password);
        }        
        
        
        if (Input::hasFile('profile_picture')) {
            $file = Input::file('profile_picture');
            $file = $file->move(public_path() . '/assets/pics/profile_picture', rand(10000, 99999) . time() . '.' . $file->getClientOriginalExtension());
            $name = explode('/', $file->getRealPath());
            $update['profile_picture'] = end($name);
            
        }
        
        User::where('id','=',$request->user()->id)
              ->update($update);
        
        return redirect()->back()->with('message','Profile is successfully updated');
        
    }
    
     public function profile($user_id) {  
        // check to see if is actual real to save it from hacking 
        $id = Auth::user()->id;
        
        if($id != $user_id){
            echo 'unauthorized access';
            exit; // means , show nothing after this
        }
         
        $data['users'] = User::find($user_id)->toArray();
        

        return view('frontend.profile',$data);
        
    }
    
    public function single(Request $request) {  
        
        $data['products'] = Product::where('product_id','=',$request->product_id)->get()->toArray();

        return view('frontend.single',$data);
        
    }
    
    
    
   
}
