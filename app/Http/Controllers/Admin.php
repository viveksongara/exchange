<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

use Illuminate\Support\Facades\Input;

use App\Category;

use App\Subcategory;

use App\Exchange;

use App\Feedback;

use App\Product;

use App\Order;


class Admin extends Controller
{
    public function index() {
        
        return view('admin.index');
        
    }
    
    public function blank() {
        
        return view('admin.blank');
        
    }
    
    public function profile($user_id) {  
        
        $data['users'] = User::find($user_id)->toArray();

        return view('admin.profile',$data);
        
    }
    
    public function update_profile(Request $request) {
        
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            
        ]);
        
        $update = array();
        $update['name'] = $request->name;
        $update['phone'] = $request->phone;
        if ($request->password != '') {
            $update['password'] = bcrypt($request->password);
        }        
        
        
        if (Input::hasFile('profile_picture')) {
            $file = Input::file('profile_picture');
            $file = $file->move(public_path() . '/assets/pics/profile_picture', rand(10000, 99999) . time() . '.' . $file->getClientOriginalExtension());
            $name = explode('/', $file->getRealPath());
            $update['profile_picture'] = end($name);
            
        }
        
        User::where('id','=',$request->user()->id)
              ->update($update);
        
        return redirect()->back()->with('message','Profile is successfully updated');
        
    }
    /////////////////////////category functoionsssssssssssssss///////////////////
    public function categories(Request $request) {
        
        $data['categories'] = Category::get()->toArray();
        
        return view('admin.categories', $data);
        
    }
    
    public function sub_categories(Request $request)  {
        
        $data['sub_categories'] = Subcategory::
                                  join('categories','categories.category_id','sub_categories.category_id')
                                  ->get()
                                  ->toArray();
        
        return view('admin.sub_categories', $data);
        
    }
    public function view_subcategory() {
         $data['subcategories']= Subcategory::get()->toArray();
        
        return view('admin.pages.view-subcategory',$data);
        
        
    }
    
    public function create_category() {
        
        
        return view('admin.pages.add-category');
        
        
    }
    
    public function creating_category(Request $request) {
        
        $this->validate($request, [
         'cat_name' => 'required'
        ]);
        
        $category = new Category();
        $category->category_name = $request->cat_name;
        
        $category->save();
        
        return redirect()->back()->with('message', 'Category is successfully created');
        
    }
    
    public function update_category(){
           
            $data['categories'] = Category::get()->toArray();
        
        
            return view('admin/pages/update-category',$data);

    }

    public function make_category_updated(Request $request){
           
          $this->validate($request,[
             'category_id' => 'required|numeric',
             'category_name' => 'required'
          ]);
        
            $update = array(
                'category_name' => $request->category_name
            );

            Category::where('cat_id','=',$request->category_id)->update($update);

            return redirect()->back()->with('message','Category is successfully deleted');

    }
    
     public function view_category() {
         $data['categories']=  Category::get()->toArray();
        
        return view('admin.pages.view-category',$data);
        
        
    }
    
    
    public function delete_category(Request $request) {
        
         Category::where('category_id','=',$request->category_id)
              ->delete();
      
        
        return redirect()->back()->with('message','Data is successfully deleted');
        
    }
    
     public function create_subcategory() {
      
         $data['categories']=  Category::get()->toArray();
         
          return view('admin.pages.add-subcategory',$data);
  
    }
    public function creating_subcategory(Request $request) {
      
         
         $subcategory=  new Subcategory;
         $subcategory->category_id=$request->category_id;
         $subcategory->subcategory_name=$request->subcategory_name;
         
        
         $subcategory->save();
         return redirect()->back()->with('message', 'Category is successfully updated');
  
    }
    
    
    
      ///////////////////////////////////////////////////////////////////
    public function view_users() {
        
        $data['users'] = User::get()->toArray();
        
        return view('admin.pages.view-user',$data);
        
    }
    public function add_users(Request $request) {
        
        // validation of the fields in the add category form
       
       $this->validate($request,[
           'name' => 'required',
           'email' => 'required',
           'country' => 'required',
           'city' => 'required',
           'state' => 'required',
           'can_accept_payment' => 'required'
           
       ]);
         $user = new User(); 
       $user->name = $request->name ;
        $user->email = $request->email ; 
         
          $user->country = $request->country ;
           $user->city = $request->city ;
            $user->state = $request->state ;
            $user->can_accept_payment = $request->can_accept_payment ;
         
       //$table_name->table ki fields=$request->form_fields name
    $user->save();
          
   
       return redirect()->back()->with('message','Data is successfully submitted');
        
        
        
    }
    
    public function change_user_status(Request $request) {
        
        $this->validate($request, [
            'user_id' => 'required|numeric',
            'status'  => $request->status
        ]);
        
        $user = User::find($request->user_id);
        $user->activation_status = $reqeust->status;
        $user->save();
        
        return redirect()->back()->with('Message', 'User Status is successfully changed');
        
    }
    
    // Note :: STILL TO BE DONE 
    public function view_all_exchanges(Request $request) {
         
    }
    
    public function transactions(Request $request) {
        
        $data['orders'] = Order::
                          join('products','orders.product_id','=','products.product_id')
                          ->join('users','users.id','=','orders.product_id')
                          ->get()->toArray();
        
        return view('admin.transactions',$data);
    }
    
    public function all_products(Request $request) {
        
        $data['products'] = Product::
                            join('users','products.vendor_id','=','users.id')
                            ->join('categories','categories.category_id','=','products.category_id')
                            ->get()
                            ->toArray();
        
        return view('admin.products', $data);
        
    }
    
    public function delete_product(Request $request) {
        
        $this->validate($request, [
        'product_id' => 'required|numeric'
        ]);
        
        Product::where('product_id','=',$request->product_id)->delete();
        
        return redirect()->back()->with('message', 'Product is successfully deleted');
    }
    
    public function update_product(Request $request) {
        
        $this->validate($request, [
           'product_id' => 'required|numeric'
        ]); 
        
        $data['products'] = Product::where('product_id','=',$request->product_id)
                            ->get()
                            ->toArray();
        
        return view('admin.update-product',$data);
        
    }
    
    
}
