<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
   protected $primaryKey = 'sub_category_id';
}
