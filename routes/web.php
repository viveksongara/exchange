<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your application. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::get('/', 'Frontend@index');

Route::get('blank', function() {
    return view('frontend.blank');
});

Route::get('signup', 'Frontend@signup');

Route::get('post-product', 'Frontend@post_product');

Route::get('export', 'Frontend@excel_test');

Route::post('user-registration', 'Frontend@user_registration');

Route::get('logout', 'Frontend@logout');

Route::get('get-all-countries', 'Frontend@get_all_countries');

Route::get('get-states', 'Frontend@get_states_by_country_id');

Route::get('get-cities', 'Frontend@get_cities_by_state_id');

Route::get('user-registration-success', function() {
    return view('frontend/registration-success');
});

Route::get('confirm-user', 'Frontend@confirm_user');

Route::get('shop', 'Frontend@shop');
Route::post('show-categories-on-shoppage', 'Frontend@show_categories_on_shoppage');
Auth::routes();

Route::get('/home', 'HomeController@index');

/////////////////////////////////// Customer/Vendor  //////////////////////////////////////

Route::group(['prefix' => 'user', 'middleware' => ['auth', 'role:1']], function () {
    Route::get('post-product', 'Frontend@post_product');
    Route::post('make-product-posted', 'Frontend@make_product_posted');
    Route::post('make-product-updated', 'Frontend@make_product_updated');
    Route::get('delete-product', 'Frontend@delete_product');
    Route::get('customer', 'Frontend@user_dashboard');
    Route::get('vendor', 'Frontend@vendor_dashboard');
    Route::get('apply-product-for-exchange', 'Frontend@apply_product_for_exchange');
    Route::get('customer-dashboard', 'Frontend@user_dashboard');
    Route::get('vendor-dashboard', 'Frontend@vendor_dashboard');
    Route::get('products-applied-for-exchange', 'Frontend@products_applied_for_exchange');
    Route::get('products', 'Frontend@products');
    Route::get('update-product', 'Frontend@update_product');
    Route::get('exchange-requests', 'Frontend@exchange_requests_to_vendor');
    Route::get('approve-exchange', 'Frontend@approve_exchange');
    Route::get('reject-exchange', 'Frontend@reject_exchange');
    Route::get('make-payment', 'Frontend@make_payment_to_admin');
    Route::get('post-product', 'Frontend@post_product');
    Route::get('get_subcategory_by_id', 'Frontend@get_subcategory_by_id');
    Route::get('item-to-swap', 'Frontend@items_to_swap');
    Route::get('item-to-exchange', 'Frontend@items_to_exchange');
    Route::get('profile/{user_id}', 'Frontend@profile');
    Route::post('update-profile', 'Frontend@update_profile');
    Route::post('make-feedback', 'Frontend@make_feedback');
    Route::get('feedback', 'Frontend@feedback');
    
});


//////////////////////////////////// Admin /////////////////////////////////////////

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'role:2']], function () {
    Route::get('/', 'Admin@index');
    Route::get('profile/{user_id}', 'Admin@profile');
    Route::post('update-profile', 'Admin@update_profile');
    Route::get('add-category', 'Admin@create_category');
    Route::post('creating-category', 'Admin@creating_category');
    Route::get('view-category', 'Admin@view_category');
    Route::get('delete-category', 'Admin@delete_category');
    Route::get('add-subcategory', 'Admin@create_subcategory');
    Route::get('update-category', 'Admin@update_category');
    Route::post('creating-subcategory', 'Admin@creating_subcategory');
    Route::get('delete-category', 'Admin@delete_category');
    Route::get('view-subcategory', 'Admin@view_subcategory');
    Route::get('view-user', 'Admin@view_users');
    Route::get('add-user', 'Admin@add_users');
});

Route::get('shop', 'Frontend@shop');
Route::get('shop-vendor', 'Frontend@show_products_on_vendor_shoppage');
Route::post('search-result', 'Frontend@search');

//////////////////////////single pages/////////////////////////
Route::get('terms', 'Frontend@terms');
Route::get('contact', 'Frontend@contact');
Route::get('page1', 'Frontend@terms');
Route::get('returns', 'Frontend@returns');
Route::get('services', 'Frontend@services');
Route::get('special', 'Frontend@terms');
Route::get('about', 'Frontend@about');
Route::get('privacy', 'Frontend@privacy');
Route::get('single', 'Frontend@single');

